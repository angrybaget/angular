Test task
 
 
Design and develop a simple system for patient - doctor communication.
 
 
User side (patient side):
 
  *   Login page
 
  *   Main page (list of available doctors, previous conversations)
 
  *   Personal data page
 
  *   Previous communication page
 
 
Patient can add/edit some personal data on Personal data page. On main page he must be able to choose doctor and send to him ticket/request with next data - problem (ticket header) and problem description (ticket body). Each user is unique and can have more than one child as a unique patient.
 
 
Doctor side:
 
  *   Login page
 
  *   Main page (list of available doctors, previous conversations)
 
  *   Personal doctor data page
 
  *   Previous communication page
 
 
Doctor is able to answer on ticket (request) from patient, can make a conversation with some other doctor about this ticket (the history of this conversation is not showed to patient) or just forward this ticket to another doctor (patient will get an information about it). Also doctor is allowed to edit/add personal data. Each doctor is unique (by username).
 
 
This system must have user friendly interface for doctor and patient and must be usable on presentation. You are allowed to use any php/css/js frameworks for this. System must support REST API (Patient can connect to system via some phone or other device).
 
 
Work result:
 
Usable site + database (required entities - Patient, Doctor).
 
 
 
Will be a plus:
 
  *   Possibility to check the list of patients which was in conversation with some doctor (tableview with possibility to change view to PDF format (for printing purposes))
 
  *   Possibility to sort and filter pervious result